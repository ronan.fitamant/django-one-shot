from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListFormCreate


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "to_do_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = TodoList.objects.get(id=id)
    context = {
        "to_do_object": todo,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            new_list = form.save()
        return redirect("todo_list_detail", id=new_list.id)

    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoListForm(instance=list)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    model_instance = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoListFormCreate(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            item = form.save()
        return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoListFormCreate()

    context = {"form": form}

    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoListFormCreate(request.POST, instance=list)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            form.save()
            return redirect("todo_list_detail", id=id)

    else:
        form = TodoListFormCreate(instance=list)

    context = {"form": form}

    return render(request, "todos/items/edit.html", context)
